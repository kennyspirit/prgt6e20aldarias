/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e20aldarias;

/**
 * Fichero: Ejercicio0604.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0604 {

  private int dato;

  Ejercicio0604(int p) {
    dato = p;
  }

  public String getNumero(String param) {
    if (param == "B") {
      return Integer.toBinaryString(dato);
    }
    if (param == "H") {
      return Integer.toHexString(dato);
    }
    if (param == "O") {
      return Integer.toOctalString(dato);
    }
    return "Parametro no reconocido";
  }

  ;

  public static void main(String[] args) {
    Ejercicio0604 s = new Ejercicio0604(20);
    System.out.println(s.dato);
    System.out.println(s.getNumero("B"));
    System.out.println(s.getNumero("H"));
    System.out.println(s.getNumero("O"));
  }
}

/* EJECUCION:
 20
 10100
 14
 24
 */
