/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e20aldarias;

/**
 * Fichero: Ejercicio0602.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0602 {

  static void pedir(String... argumentos) {
    for (String str : argumentos) {
      System.out.println(str);
    }
  }

  public static void main(String[] args) {
    pedir("mama pipi", "mama caca", "mama agua");
    pedir(new String[]{"papa jugar", "mama me aburro", "papa sed",
      "papa dormir", "mama tengo hambre"});
  }
}
