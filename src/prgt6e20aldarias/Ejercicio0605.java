/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e20aldarias;

/**
 * Fichero: Ejercicio0605.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0605 {

  public String normalToAmericano(String s) {
    return s.substring(3, 5) + "/" + s.substring(0, 2) + "/" + s.substring(6);
  }

  public String americanoToNormal(String s) {
    return s.substring(3, 5) + "/" + s.substring(0, 2) + "/" + s.substring(6);
  }

  public static void main(String[] args) {
    Ejercicio0605 s = new Ejercicio0605();
    System.out.println(s.americanoToNormal("08/16/1973"));
    System.out.println(s.normalToAmericano("16/08/1973"));
  }
}

/* EJECUCION:
 08/16/1973
 16/08/1973
 */
