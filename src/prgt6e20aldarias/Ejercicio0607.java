/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e20aldarias;

/**
 * Fichero: Ejercicio0607.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
// ***************** Clase Vehiculo
abstract class Vehiculo {

  private int peso;

  public final void setPeso(int p) {
    peso = p;
  }

  public final int getPeso() {
    return peso;
  }

  public abstract int getVelocidadActual();
}

// ***************** Clase coche
class Coche extends Vehiculo {

  Coche(int p) {
    setPeso(p);
  }

  public int getVelocidadActual() {
    return 33;
  }
}

// ***************** Main
class Ejercicio0607 {

  public static void main(String[] args) {
    Coche c = new Coche(1500);
    System.out.println(c.getPeso());
    System.out.println(c.getVelocidadActual());
  }
}
/* EJECUCION:
 1500
 33
 */
