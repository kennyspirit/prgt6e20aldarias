/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt6e20aldarias;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: Ejercicio0608.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0608 {

  int num1, num2;

  public void leeNumeros() {
    InputStreamReader isr = new InputStreamReader(System.in);
    BufferedReader buff = new BufferedReader(isr);
    try {
      System.out.print("Introduzca el primer numero:");
      String ln = buff.readLine();
      num1 = Integer.parseInt(ln); // wrapper
      System.out.print("Introduzca el segundo numero:");
      ln = buff.readLine();
      num2 = Integer.parseInt(ln); // wrapper
    } catch (IOException e) {
      System.err.println("Se ha producido una IOException");
      e.printStackTrace();
    } catch (Throwable e) {
      System.err.println("Error de programa: " + e);
      e.printStackTrace();
    }
  }

  public void muestraNumeros() {
    try {
      System.out.println(num1 + num2);
      System.out.println(num1 * num2);
      System.out.println(num1 / num2);
      System.out.println(num1 % num2);
    } catch (ArithmeticException e) {
      System.out.println("Division por cero");
    } catch (Exception e) {
      System.out.println("Se produjo un error en el programa");
    }
  }

  public static void main(String[] args) {
    Ejercicio0608 t = new Ejercicio0608();
    t.leeNumeros();
    t.muestraNumeros();
  }
}

/* EJECUCION:
 Introduzca el primer numero:2
 Introduzca el segundo numero:2
 4
 4
 1
 0
 */
